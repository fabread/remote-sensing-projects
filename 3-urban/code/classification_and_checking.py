import matplotlib.pyplot as plt
import matplotlib.image as mpimg
from PIL import Image
import numpy as np
from math import *
from scipy import ndimage

###Classification
date = []
#Loading of the images

path = "image_folder_path"

date1 = ["04_05_2020_reproj/2020-05-04, Sentinel-2A L1C,"]    #Let's compare the classification of 5 dates ranged on a short period (1 month)
date2 = ["07_05_2020_reproj/2020-05-07, Sentinel-2A L1C,"]
date3 = ["09_05_2020_reproj/2020-05-09, Sentinel-2B L1C,"]
date4 = ["19_05_2020_reproj/2020-05-19, Sentinel-2B L1C,"]
date5 = ["22_05_2020_reproj/2020-05-22, Sentinel-2B L1C,"]

date += date1
date += date2
date += date3
date += date4
date += date5


TrueColor_PIL = Image.open(path +  date1[0] + ' True color_reproj.tiff')     #Open the image as a PIL image
Res = np.copy(np.array(TrueColor_PIL))      
Res2 = np.array(TrueColor_PIL).tolist()     #Res2 is a list with the size of the image matrix which will gather all the classifications for each date, in a list, for each pixel
Final = np.array(TrueColor_PIL)             #Final is the image reconstituted from selecting the maximum occurence for each pixel from the lists of Res2 

for i in range(len(Res2)):
    for j in range(len(Res2[0])):
        Res2[i][j] = []

for d in date :
    plt.figure()
    r_PIL = Image.open(path + d + ' B04_reproj.tiff')
    r = np.array(r_PIL) # Transformation of the image into a numpy array, here the red band
    NIR_PIL = Image.open(path +  d + ' B08_reproj.tiff')
    NIR = np.array(NIR_PIL) #Near Infra-Red
    b_PIL = Image.open(path +  d + ' B02_reproj.tiff')
    b = np.array(b_PIL) #Blue
    MIR_PIL = Image.open(path +  d + ' B11_reproj.tiff')
    MIR = np.array(MIR_PIL) # "Middle IR" generally called SWIR (short wave IR)
    TrueColor_PIL = Image.open(path +  d + ' True color_reproj.tiff')
    TrueColor = np.array(TrueColor_PIL)
    G_PIL = Image.open(path +  d + ' B03_reproj.tiff')
    G = np.array(G_PIL) # Green
    
    for i in range (TrueColor.shape[0]):
        for j in range (TrueColor.shape[1]):
            NDVI=(NIR[i][j]-r[i][j])/(NIR[i][j]+r[i][j])            # Vegetation Index calculation 
            NDWI=(G[i][j]-NIR[i][j])/(G[i][j]+NIR[i][j])            # Water Index calculation 
            NDBI=(MIR[i][j]-NIR[i][j])/(MIR[i][j]+NIR[i][j])            # Building Index calculation 
            
            
            if ((0.5<NDVI<1 and 0<NDBI<0.5) or (0.5<NDVI<1 and 0<NDWI<1) or (0<NDWI<1 and 0<NDBI<0.5)):
                Res[i][j][0]=2                                                                    # Black Pixels which represent the pixels belonging to
                Res[i][j][1]=0                                                                        # more than 1 class
                                                                                                    # They are added to Res2 with the designation of the Red value (here '2')
                Res[i][j][2]=1
                Res2[i][j].append(2)
            
            elif 1>NDVI>0.7:
                Res[i][j][0]=40                                                                # Green pixels describing vegetation        
                Res[i][j][1]=119
                Res[i][j][2]=52
                Res2[i][j].append(40)                                                            # They are added to Res2 with the designation of the Red value (here '40')
            
            elif 1>NDVI>0.5:
                Res[i][j][0]=101                                                                  # Green pixels describing vegetation        
                Res[i][j][1]=214
                Res[i][j][2]=0
                Res2[i][j].append(101)                                                            # They are added to Res2 with the designation of the Red value (here '101')
                
            
            elif 0<NDBI<0.5:
                Res[i][j][0]=214                                                                  # Red pixels describing the building areas (and nude soil areas)
                Res[i][j][1]=0
                Res[i][j][2]=0
                Res2[i][j].append(214)                                                            # They are added to Res2 with the designation of the Red value (here '214')
    
            
            elif 0<NDWI<1:
                Res[i][j][0]=0                                                                  # Blue pixels describing the water areas
                Res[i][j][1]=0
                Res[i][j][2]=214
                Res2[i][j].append(0)                                                            # They are added to Res2 with the designation of the Red value (here '0')
                
                
            else :
                Res[i][j][0]=100                                                                  # Purple pixels describing the non classed pixels, containing roads and fields
                Res[i][j][1]=0
                Res[i][j][2]=100
                Res2[i][j].append(100)                                                            # They are added to Res2 with the designation of the Red value (here '100')
                

    plt.imshow(Res)
    
Classification = Image.fromarray(Res) # Transformation of the array into a PIL image
Classification.save("image_saving_path.png") #result saving


plt.figure()
for i in range (len(Res2)):
    for j in range (len(Res2[0])):
        n1 = Res2[i][j].count(2)      #count the number of the 'multi classification' class for each pixel
        n2 = Res2[i][j].count(101)    #count the number of the 'Vegetation' class for each pixel
        n3 = Res2[i][j].count(214)    #count the number of the 'Building' class for each pixel
        n4 = Res2[i][j].count(0)      #count the number of the 'Water' class for each pixel
        n6 = Res2[i][j].count(100)    #count the number of the 'Non-classed' class for each pixel
        n8 = Res2[i][j].count(40)     #count the number of the 'Less Vegetation' class for each pixel
        L = [n1,n2,n3,n4,n6,n8]

        m = 0
        m = max(L)  #We find the maximum in this list, the case of multiple maxima is discussed in the report
           
        

        if m == n8 :
            Final[i][j][0]=40         # Green pixels describing dense vegetation       
            Final[i][j][1]=119
            Final[i][j][2]=52                                     
          
        elif m == n2:
            Final[i][j][0]=101       # Retranscription of the diffuse vegetation in the Final image
            Final[i][j][1]=214
            Final[i][j][2]=0  
         
           
        elif m == n3:
            Final[i][j][0]=214       # Retranscription of the buildings in the Final image
            Final[i][j][1]=0
            Final[i][j][2]=0
            
        elif m == n4:
            Final[i][j][0]=0       # Retranscription of the water in the Final image
            Final[i][j][1]=0
            Final[i][j][2]=214
            
            
        elif m == n6:
            Final[i][j][0]=100       # Retranscription of the non-classed areas in the Final image
            Final[i][j][1]=0
            Final[i][j][2]=100
            
        elif :
            Final[i][j][0]=2       # Retranscription of the multi-classed areas in the Final image
            Final[i][j][1]=1
            Final[i][j][2]=1   
            
        
plt.imshow(Final)
plt.show()

## Accuracy testing

ref_PIL = Image.open('path_to_reference_image')
ref = np.array(ref_PIL) #ref is the reference image used for accuracy testing

im_PIL = Image.open('path_to_result_image')
im = np.array(im_PIL) #im is the result ouf our classification

non_classified=0
rigtly_classified=0
wrongly_classified=0

for i in range(im.shape[0]):
    for j in range(im.shape[1]):
        pixel=[im[i][j][0],im[i][j][1],im[i][j][2]]
        pixel_ref=[ref[i][j][0],ref[i][j][1],ref[i][j][2]]
        
        if pixel == [100,0,100] or pixel == [2,1,1]: #count the number of non classified or multi classified pixels
            wrongly_classified+=1
       
        elif pixel == [214,0,0]: #built up
            if pixel_ref == [255,170,255] or pixel_ref == [255,85,255]:
                rigtly_classified+=1
            else: wrongly_classified+=1
        
        elif pixel == [0,0,214]: #water
            if pixel_ref == [0,0,255]:
                rigtly_classified+=1
            else: wrongly_classified+=1
        
        else : #vegetation
            if pixel_ref == [0,156,0] or pixel_ref == [170,255,0] or pixel_ref == [170,170,0] or pixel_ref == [0,50,0] or pixel_ref == [255,255,127] or pixel_ref == [255,85,0]:
                rigtly_classified+=1
            else: wrongly_classified+=1
        


