(1) To begin with, you need the full data folders available at : https://www.dropbox.com/sh/0shjz7amy3qqb4j/AAAt78XQ_6DLSmqI9FBktLwwa?dl=0
(2) The files in lidar_features\raster have been slightly modified to include a "plotid" column in each. Therefore, you need to erase the "raster" folder
initially provided and replace it with the "raster" folder provided in this zip archive.
(3) Run the CSV_total.py script: it will generate the CSV file used to train the Random Forest Agorithm. It might take a while.
(4) After that, you (might) need to delete the line 12853 of the CSV file because of an error. (specie = 'ND')
(5) To run the RF algorithm and display the classification map, run the RandomForest.py script. 

Note : 'whittaker.py' and 'tif_picture_to_dataframe' should not be modified. All the python file need to be at the same level that the 'gt', 'hi', 'lidar' 
and 'lidar_features' folders.


NEEDED MODULES : pandas, numpy, sklearn, tifffile, matplotlib (+eventually seaborn)